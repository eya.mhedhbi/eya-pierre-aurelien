# L2_CDA_ModuleImage

Description:

	Ce projet concerne le module Image développé dans le cadre du cours "LIFAPCD Conception et développement
	d'application" à l'Université Lyon 1. Le module Image comprend des fonctionnalités de manipulation
	d'images, telles que lacréation, la modification, et l'affichage.

Membres du groupe:

   MHEDHBI EYA p2210828 /
   DUCHENE AURELIEN p2205507  /
   BONFILS PIERRE p2203540
   
Organisation de l'archive:

    
    L'archive du projet est structurée comme suit :
    bin: Ce répertoire contiendra les fichiers exécutables générés après la compilation.
    src: Contient les fichiers source du module Image, y compris les fichiers de la classe Image,
    les structures Pixel, ainsi que les fonctionnalités associées.
    obj: Ce répertoire contiendra les fichiers objets générés lors de la compilation.
    data: Contient les données nécessaires aux programmes, telles que les images utilisées pour les tests.
    doc: Contiendra la documentation générée par Doxygen pour le module Image.
    Cette structure permet une organisation claire des fichiers sources, des exécutables,
    des données de test et de la documentation.
    
Pour compiler le module Image et ses exécutables associés, suivez ces étapes :

    Assurez-vous d'avoir les dépendances nécessaires installées, telles que SDL2.
    Naviguez vers le répertoire racine du projet.
    Utilisez le Makefile fourni pour compiler le module et les exécutables : 
        make
    une fois la compilation terminée avec succès, vous pouvez exécuter les programmes comme suit :

    Pour exécuter le programme principal mainExemple.cpp : 
        bin/exemple

    Pour exécuter le programme principal mainAffichage.cpp :
        bin/affichage

Contenu des exécutables

    exemple: Ce programme démontre l'utilisation de diverses fonctionnalités du module Image, telles que 
    la création d'une image, le dessin de rectangles, la sauvegarde et l'ouverture d'images à partir de fichiers.
    affichage: Ce programme utilise la classe ImageViewer pour afficher une image créée avec le module Image en
    utilisant SDL2. Il permet également de zoomer/dézoomer sur l'image et de quitter l'affichage avec la touche
    ESCAPE.
    
Projet sur la forge : https://forge.univ-lyon1.fr/eya.mhedhbi/eya-pierre-aurelien.git
