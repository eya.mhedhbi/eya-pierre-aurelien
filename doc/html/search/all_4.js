var searchData=
[
  ['g_0',['g',['../structPixel.html#a8407845aacf1663d9463475619911686',1,'Pixel']]],
  ['getdimx_1',['getDimX',['../classImage.html#a5eb08fc4d14fa9933aa2fd64380eca98',1,'Image']]],
  ['getdimy_2',['getDimY',['../classImage.html#a9c46d1bf18fa88b97a6876054f74b795',1,'Image']]],
  ['getpix_3',['getPix',['../classImage.html#a77a8d8824141ed8b9db62154a211536d',1,'Image::getPix(int x, int y)'],['../classImage.html#a99be5d70422354999a5f63da4cb85838',1,'Image::getPix(int x, int y) const']]],
  ['gettab_4',['getTab',['../classImage.html#a07780f5b37697f1b69a00c843b542dd9',1,'Image']]]
];
