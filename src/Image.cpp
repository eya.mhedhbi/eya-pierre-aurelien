#include <iostream>
#include <cassert>
#include "Image.h"
#include "Pixel.h"
#include <fstream>

Image::Image()
{
    dimx=0;
    dimy=0;
    tab = nullptr;
}

Image::Image(unsigned int dimensionX, unsigned int dimensionY)
{
    dimx=dimensionX;
    dimy=dimensionY;
    //tab.resize(dimx * dimy);
    tab = new Pixel[dimx*dimy];
}

Image::~Image() 
{
    //tab.clear();
    if (tab!=nullptr)
        delete [] tab;
    tab = nullptr;
    dimx = 0;
    dimy = 0;
}

bool Image::isValid(int x, int y) const
{
    return (x >= 0 && x < dimx && y >= 0 && y < dimy);
}

void Image::testRegression()
{
    // Création d'une image vide
    Image img1;
    assert(img1.getDimX() == 0);
    assert(img1.getDimY() == 0);

    // Création d'une image avec des dimensions spécifiées
    Image img2(20,20);
    assert(img2.getDimX() == 20);
    assert(img2.getDimY() == 20);

    // Vérification des valeurs des pixels
    assert(img2.getPix(10,10).r == 0);
    assert(img2.getPix(10,10).g == 0);
    assert(img2.getPix(10,10).b == 0);

    // Modification d'un pixel et vérification
    Pixel p(1,1,1);
    img2.setPix(10,10,p);
    assert(img2.getPix(10,10).r == 1);
    assert(img2.getPix(10,10).g == 1);
    assert(img2.getPix(10,10).b == 1);

    // Création d'une nouvelle image et effacement
    Image img(20,20);
    Pixel t(1,1,1);
    img.effacer(t);

    // Dessin d'un rectangle
    Pixel couleurRectangle(8, 8,8);
    img.dessinerRectangle(4, 4, 18, 18, couleurRectangle);

    // Vérification des valeurs des pixels après dessin du rectangle
    for (int x = 0; x < 20; ++x) {
        for (int y = 0; y < 20; ++y) {
            if (x >= 4 && x <= 18 && y >= 4 && y <= 18) {
                assert(img.getPix(x, y).r == 8);
                assert(img.getPix(x, y).g == 8);
                assert(img.getPix(x, y).b == 8);
            } else {
                assert(img.getPix(x, y).r == 1);
                assert(img.getPix(x, y).g == 1);
                assert(img.getPix(x, y).b == 1);
            }
        }
    }

    std::cout << "Tests de regression réussis." << std::endl;
}


Pixel&  Image::getPix (int x,int y) 
{
    assert(isValid(x,y));
    return tab[y*dimx+x]; 

}

Pixel  Image::getPix(int x, int y) const
{
    assert(isValid(x,y));
    return tab[y*dimx+x]; 
}

void Image::setPix (int x, int y, Pixel couleur)
{
    assert(isValid(x,y));
    tab[y*dimx+x]=couleur;
}


void Image::dessinerRectangle(int Xmin, int Ymin, int Xmax, int Ymax, Pixel couleur)
{
    for (int y = Ymin; y <= Ymax; ++y) {
        for (int x = Xmin; x <= Xmax; ++x) {
            Pixel p = getPix(x, y);
            p = couleur;
            setPix(x, y, p);
        }
    }
}

void Image::effacer(Pixel couleur)
{
    dessinerRectangle(0,0,dimx-1,dimy-1,couleur);
}



void Image::sauver(const string &filename) const
{
    ofstream fichier(filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << endl;
    fichier << dimx << " " << dimy << endl;
    fichier << "255" << endl;
    for (unsigned int y = 0; y < dimy; ++y)
        for (unsigned int x = 0; x < dimx; ++x)
        {
            Pixel pix = getPix(x, y);
            fichier << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

void Image::ouvrir(const string &filename)
{
    ifstream fichier(filename.c_str());
    assert(fichier.is_open());
    char r, g, b;
    string mot;
    dimx = dimy = 0;
    fichier >> mot >> dimx >> dimy >> mot;
    assert(dimx > 0 && dimy > 0);

    if (tab) delete [] tab;
    tab = new Pixel[dimx*dimy];

    //if (!tab.empty())
    //    tab.clear();
    //tab.resize(dimx*dimy);

    for (unsigned int y = 0; y < dimy; ++y)
        for (unsigned int x = 0; x < dimx; ++x)
        {
            fichier >> r >> g >> b;
            getPix(x, y).r = r;
            getPix(x, y).g = g;
            getPix(x, y).b = b;
        }
    fichier.close();
    cout << "Lecture de l'image " << filename << " ... OK\n";
}

void Image::afficherConsole()
{
    cout << dimx << " " << dimy << endl;
    for (unsigned int y = 0; y < dimy; ++y)
    {
        for (unsigned int x = 0; x < dimx; ++x)
        {
            Pixel &pix = getPix(x, y);
            cout << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
        cout << endl;
    }
}

int Image::getDimX() const {
    return dimx;
}

int Image::getDimY() const {
    return dimy;
}

const Pixel* Image::getTab() const {
    return tab; //.data();
}





