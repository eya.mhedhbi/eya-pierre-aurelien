#ifndef _PIXEL_H
#define _PIXEL_H

/**
 * @struct Pixel
 * @brief Represents a pixel with red, green, and blue components.
 */
struct Pixel 
{
    unsigned char r; /**< Red component of the pixel. */
    unsigned char g; /**< Green component of the pixel. */
    unsigned char b; /**< Blue component of the pixel. */

    /**
     * @brief Default constructor initializing all components to 0.
     */
    Pixel () : r(0), g(0), b(0) {}

    /**
     * @brief Constructor initializing the pixel with given color components.
     * @param nr Red component.
     * @param ng Green component.
     * @param nb Blue component.
     */
    Pixel (unsigned char nr, unsigned char ng, unsigned char nb) : r(nr) , g(ng) , b(nb) {}

};

#endif
