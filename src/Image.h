#ifndef _IMAGE_H
#define _IMAGE_H
#include <string>
//#include <vector>
#include "Pixel.h"
using namespace std;

/**
 * @class Image
 * 
 * @brief this class represents an image
 * 
 * The Image class contains functionalities for managing images.
*/

class Image
{
    private:
        int dimx,dimy;///<these are the Width and height of the image 
        Pixel* tab;///< this is the array of pixels that represents the image  
    
    public:
    

    /**
     * @brief default constructer of the class image
     * 
     * initializes dimx and dimy to 0 and allocates no memory for the pixel array.
     */

    Image();


    /**
     * @brief  constructer of the class image with specified dimensions
     * 
     * initializes dimx and dimy after validation and allocate memory for the pixel array with a black picture
     * 
     * @param dimensionX The width of the image.
     * @param dimensionY the height of the image
     */
    
    Image (unsigned int dimensionX, unsigned  int dimensionY);


    /**
     * @brief destructer of the class image
     * 
     * Deallocates the pixel array and updates dimx and dimy to 0
    */

    ~Image ();


    /**
     * @brief checks if the given coordinates are valid within the image
     * 
     * @param x the x coordinate
     * @param y the y coordinate
     * 
     * @return true if the coordinates are valide, false otherwise
    */

    bool isValid(int x, int y)const;


    /**
     * @brief static method to perform regression testing for the image class
    */

    static void testRegression();


    /**
     * @brief fetches the original pixel at the given coordinates (after validation of the coordinates)
     * 
     * @param x the x coordinate
     * @param y the y coordinate
     * 
     * @return reference to the original pixel at the coordinates (x,y)
    */

    Pixel&  getPix (int x,int y);


    /**
     * @brief fetches the original pixel at the given coordinates (after validation of the coordinates) (const version)
     * 
     * @param x the x coordinate
     * @param y the y coordinate
     * 
     * @return to the original pixel at the coordinates (x,y)
    */
    Pixel  getPix(int x, int y) const;


    /**
     * @brief changes the pixel at the given coordinates (after validation) with the given pixel
     * 
     * @param x the x coordinate 
     * @param y the y coordinate 
     * @param couleur the pixel used to change the old one 
    */
    void setPix (int x, int y, Pixel couleur);


    /**
     * @brief draws a rectangle filled with the given pixel in the Image within the right coordinates
     * 
     * @param Xmin The minimum x-coordinate of the rectangle.
     * @param Ymin The minimum y-coordinate of the rectangle.
     * @param Xmax The maximum x-coordinate of the rectangle. 
     * @param Ymax The maximum y-coordinate of the rectangle.
    */
    void dessinerRectangle(int Xmin,int Ymin,int Xmax,int Ymax,Pixel couleur);
    // Dessine un rectangle plein de la couleur dans l'image
    // (en utilisant setPix, indices en paramètre compris)


    /**
     * @brief Clears the image by filling it with the specified color.
     * 
     * calls dessinerRectangle with appropriate rectangle coordinates
     * 
     * @param couleur the color to fill the image with 
    */
    void effacer(Pixel couleur);


    /**
     * @brief saves the image to a file
     *
     * saves the image in the specified file
     * 
     * @param filename the name of the file to save the image to
    */
    void sauver(const string &filename)const;


    /**
     * @brief opens an image from a file and replace the current image with the loaded one from the file
     * 
     * Opens the specified file, reads the image data, allocates memory for the pixel array,
     *  stores pixel values, closes the file, and prints a confirmation message.
     * 
     * @param filename the name of the file to open the image from
    */
    void ouvrir(const string &filename);


    /**
    * @brief Displays the pixel values on the console.
    */
    void afficherConsole();

    /**
     * @brief Obtient la largeur de l'image.
     * 
     * @return La largeur de l'image.
     */
    int getDimX() const;

    /**
     * @brief Obtient la hauteur de l'image.
     * 
     * @return La hauteur de l'image.
     */
    int getDimY() const;

    /**
     * @brief Obtient une référence constante vers le tableau de pixels.
     * 
     * @return Une référence constante vers le tableau de pixels.
     */
    const Pixel* getTab() const;
};



#endif