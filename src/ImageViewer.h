#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <SDL2/SDL.h>
#include "Image.h"

/**
 * @brief A class for displaying images using SDL.
 */
class ImageViewer {
private:
    SDL_Window* window; /**< Pointer to SDL_Window */
    SDL_Renderer* renderer; /**< Pointer to SDL_Renderer */
    SDL_Surface * surface; /**< Pointer to SDL_Surface */
    SDL_Texture * texture; /**< Pointer to SDL_Texture */
    SDL_Rect rect; /**< SDL_Rect structure for rendering */


public:
    /**
     * @brief Constructor for ImageViewer class.
     */
    ImageViewer();


    /**
     * @brief Destructor for ImageViewer class.
     */
    ~ImageViewer();
    

    /**
     * @brief Displays the image passed as a parameter and allows (de)zooming.
     * 
     * @param im The image to be displayed.
     */
    void afficher(const Image& im);


    /**
    * @brief Update the display with the given image.
    *
    * This function updates the display with the provided image.
    *
    * @param im The image to be displayed.
    */
    void aff(const Image& im);

    /**
    * @brief Updates the size of the image.
    *
    * This function updates the size of the image based on the provided parameters.
    *
    * @param im The image to be resized.
    * @param emX The new size in the X dimension.
    * @param emY The new size in the Y dimension.
    * @param tX The transformation factor for the X dimension.
    * @param tY The transformation factor for the Y dimension.
    */
    void updateSize(const Image& im, int emX, int emY, int tX, int tY);


};


#endif
