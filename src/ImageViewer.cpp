#include "ImageViewer.h"
#include <assert.h>
#include <iostream>
using namespace std;

ImageViewer::ImageViewer(): window(nullptr), renderer(nullptr), texture(nullptr), surface(nullptr), rect{50,50,0,0}
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << endl;
        SDL_Quit();
        exit(1);
    }

    window = SDL_CreateWindow("ImageViewer", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 200, 200, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (window == nullptr) {
        cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << endl; 
        SDL_Quit(); 
        exit(1);
        assert(window == nullptr);   // Le assert permet de retrouver l'erreur plus rapidement que juste avec le cout
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == nullptr) {                 // test si renderer a fonctionné
        SDL_DestroyWindow(window);
        SDL_Quit();        
        assert(!renderer);          // Le assert permet de retrouver l'erreur plus rapidement que juste avec le cout
    }
    
    // Effacer le rendu actuel avec une couleur de gris clair (RGB : 200, 200, 200)
    SDL_SetRenderDrawColor(renderer, 200, 200, 200, 255);
    SDL_RenderClear(renderer);
}

ImageViewer::~ImageViewer()
{
    if (window) {
        SDL_DestroyWindow(window);
        window = nullptr;
    }

    if (renderer != nullptr) {
        SDL_DestroyRenderer(renderer);
        renderer = nullptr;
    }
    if (texture != nullptr) {
        SDL_DestroyTexture(texture);
        texture = nullptr;
    }
    SDL_Quit();
}

void ImageViewer::afficher(const Image &im) {

    SDL_Event events;
    int emX = 0;
    int emY = 0;
    int tX = 0;
    int tY = 0;
    int width;
    int height;
    bool quit = false;
    while (!quit) {
        while (SDL_PollEvent(&events))
        {
            if (events.type == SDL_QUIT) {
                quit = true;
            } 
            else if (events.type == SDL_WINDOWEVENT && events.window.event == SDL_WINDOWEVENT_EXPOSED) {
                updateSize(im, emX, emY, tX, tY);
            }
            else if (events.type == SDL_KEYDOWN) {
                switch (events.key.keysym.scancode)
                {
                    case SDL_SCANCODE_ESCAPE:
                        quit = true;
                        break;
                    case SDL_SCANCODE_G:
                        tX += 10;
                        tY += 10;
                        emX -= 5;
                        emY -= 5;
                        updateSize(im, emX, emY, tX, tY);
                        break;
                    case SDL_SCANCODE_T:
                        tX -= 10;
                        tY -= 10;
                        emX += 5;
                        emY += 5;
                        updateSize(im, emX, emY, tX, tY);
                        break;
                    default: break;
                }
            }
        }
    }
}

void ImageViewer::aff(const Image& im)
{

    SDL_SetRenderDrawColor(renderer, 200, 200, 200, 255);
    SDL_RenderClear(renderer);
    surface = SDL_CreateRGBSurfaceFrom((void*)im.getTab(), im.getDimX(), im.getDimY(), 24, im.getDimX() * 3, 0x0000FF, 0x00FF00, 0xFF0000, 0);
    SDL_Surface * surfaceCorrectPixelFormat = SDL_ConvertSurfaceFormat(surface, SDL_PIXELFORMAT_ARGB8888, 0);
    assert(surface != nullptr);
    texture = SDL_CreateTextureFromSurface(renderer, surfaceCorrectPixelFormat);
    SDL_RenderCopy(renderer, texture, nullptr, &rect);
}

void ImageViewer::updateSize(const Image& im, int emX, int emY, int tX, int tY)
{
    int width, height;
    SDL_GetWindowSize(window, &width, &height);
    rect.w = im.getDimX() + tX;
    rect.h = im.getDimY() + tY;
    rect.x = (width - im.getDimX()) / 2 + emX;
    rect.y = (height - im.getDimY()) / 2 + emY;
    aff(im);
    SDL_RenderPresent(renderer);

}
